﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_4_RPOON
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
